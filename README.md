# Pim
**Pim** is a minimalistic package manager for UNIX-like operating systems.

```sh
pim -p        # Pull updates for all repos
pim -u        # Upgrade all packages to the latest version
pim -v        # Shows information about PIM
pim -h        # Show help message
pim -e        # Prepares environment variables

pim -R <repo>   # Adds a repo
pim -Rr <repo>  # Removes a repo
pim -Rl         # Lists repos

pim <pkg>       # Installs or upgrades a package (or packages)
pim -r <pkg>    # Removes a package (or packages)
pim -l          # Lists installed packages
pim -q 'abc'    # Searches for a pacakge

pim -s <pkg>    # Opens a temporary shell with a specific set of packages installed
```

## Environment Variables
| Environment Variable |  Type  |        Value        |
|----------------------|--------|---------------------|
| PIM_COLOR            | Bool   | 1 (Yes)             |
| PIM_QUIET            | Bool   | 0 (No)              |
| PIM_TMP              | Dir    | /tmp/.pim-tmp/      |
| PIM_ROOT             | Dir    | /opt/pim/           |

## Package Name Conventions
Package names should use alphanumeric characters and the dash (`-`) character. Development versions of packages should end in `-dev`. Binary packages, ones that are not built at install time, should end in `-bin`.

## Packages
```sh
VERSION="1.0"       # The package version
NAME="foo-bin"      # The actual name of this package
PACKAGE="foo"       # The secondary name used for dependency resolution (alternates/variants)
DEPENDENCIES="bar"  # Space-separated list of dependencies, preferably in the secondary name form instead of the actual name form unless required

# This function will be called to install the package
install() {
  touch $PIM_ROOT/.foo
}

# This function will be called to remvoe the package
remove() {
  rm $PIM_ROOT/.foo
}
```

## Directory Structure
```
/opt/pim/
 ├ pim_installed
 ├ pim_repos
 ├ repo/
 │   └ myrepo/
 │       ├ pkg/
 │       │   └ package
 │       └ bin/
 │           └ foobar.bin
 │
 ├ bin/
 ├ lib/
 └ ...
```
